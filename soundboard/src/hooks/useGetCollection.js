import { useEffect, useState } from "react"
import axios from "axios";

axios.defaults.baseURL = "http://localhost:3000";

export function useGetCollection(url) {
  const [loading, setIsLoading] = useState(true)
  const [fetchedData, setFetchedData] = useState([])
  async function refetch() {
    setIsLoading(true)
    const { data } = await axios.get(url)
    setFetchedData(data)
    setIsLoading(false)
    return { loading, "data": fetchedData, refetch }
  }
  useEffect(() => {
    async function getData() {
      const { data } = await axios.get(url)
      setFetchedData(data)
      setIsLoading(false)
    }
    getData()
    return { loading, "data": fetchedData, refetch }
  }, [url]);
  return { loading, "data": fetchedData, refetch }
}