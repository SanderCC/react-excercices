import * as React from 'react';
import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';

export default function SmallAlert(severity, content) {
  return (
    <Stack sx={{ width: 'fit-content', m: "auto" }} spacing={2}>
      <Alert severity={severity}>{content} — check it out!</Alert>
    </Stack>
  );
}