import { useGetCollection } from './../hooks/useGetCollection'
import { SoundboardProps } from './ISoundboard'
import { SoundboardCard } from "./SoundboardCard"
import "../css/Soundboards.css"
import Alert from "./Alert"

export function Soundboards() {
  const { loading, data: soundboards } = useGetCollection("/soundboards") 
  if(loading) return Alert("info", "Loading...")

  return (
    <div className="soundboards">
      {
        soundboards.map((soundboard : SoundboardProps) => (
          <SoundboardCard id={soundboard.id} name={soundboard.name} image={soundboard.image} />
        ))
      }
    </div>)
}

export default Soundboards