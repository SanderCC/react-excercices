import { useState } from "react"
import { SoundboardItemProps } from './ISoundboard'

export function SoundboardItem({ item, onSoundStop, onSoundStart }: SoundboardItemProps) {
  const audio = new Audio("../../"+item.sound);
  audio.addEventListener("ended", () => {
    setIsPlaying(false);
    onSoundStop();
  });
  const [isPlaying, setIsPlaying] = useState(false);
  return (
    <div 
      className={`item ${isPlaying ? "playing" : ""}`} 
      onClick={() => playSound()}
    >
      <img alt={item.name} src={"../../"+item.image} />
    </div>
  );

  function playSound() {
    if(!isPlaying) {
      setIsPlaying(true)
      audio.play()
      onSoundStart(item.quote)
    }
  }
}

export default SoundboardItem