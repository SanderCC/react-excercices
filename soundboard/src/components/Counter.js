import { useState } from "react"
import "../css/counter.css"

function Counter() {
  const [counter, setCounter] = useState(0)

  return (
    <div className={counter % 2 ? "counter" : "counter even"}>
      <button onClick={() => setCounter(counter+1)}>Counted: {counter}</button>
    </div>
  )
}

export default Counter