export interface SoundboardProps {
  id: number;
  name: string;
  image: string;
}

export interface Item {
  id: number;
  name: string;
  quote: string;
  image: string;
  sound: string;
  soundboardId: number;
}

export interface SoundboardItemProps {
  item: Item;
  onSoundStart: (quote: string) => void;
  onSoundStop: () => void;
}

export type ItemFormData = {
  name: string,
  quote: string,
  image: string,
  sound: string,
  soundboardId: number,
}

export type ItemFormProps = {
  onSubmit: (formData: ItemFormData) => void;
  onCancel: () => void;
};

export interface SettingProps {
  useNames: boolean,
  useQuotes: boolean,
  setUseNames: any,
  setUseQuotes: any,
}