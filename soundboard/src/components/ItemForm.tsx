import axios from 'axios'
import "../css/Form.css"
import { useState } from "react"
import * as React from 'react';
import TextField from '@mui/material/TextField';
import { ItemFormData } from './ISoundboard';
import Alert from "./Alert"


async function addItem(item: ItemFormData) {
  return await axios.post(`/items`, item);
}

interface FormProps  {
  soundboard: any;
  refetch: any;
  items: any;
  setShowForm: any;
}



export function ItemForm({ soundboard, refetch, setShowForm }:FormProps) {
  const [formData, setFormData] = useState<ItemFormData>({
    name: "",
    quote: "",
    image: "",
    sound: "",
    soundboardId: soundboard,
  });

  function handleChange(property: string, value: string) {
    setFormData({ ...formData, [property]: value });
  }
    

  const [error, setError] = useState('')

  async function submitHandler(event : any) {
    event.preventDefault();
    if (formData.name === '' || formData.image === '' || formData.quote === '') {
      setError("Please fill in correct data only.")
      return
    }
    await addItem(formData)
    await refetch()
    setShowForm(false)
  }

  /*function onChangeHandler(event) {
    
  }*/

  return (
    <>
      {ShowError(error)}
      <form onSubmit={submitHandler} >
        <h3>New item</h3>
        <TextField sx={{ margin: 1 }} label="Name" variant="standard" value={formData.name} onChange={(event) => handleChange("name", event.target.value)} /><br />
        <TextField sx={{ margin: 1 }} label="Quote" variant="standard" value={formData.quote} onChange={(event) => handleChange("quote", event.target.value)} /><br />
        <TextField sx={{ margin: 1 }} label="Image" variant="standard" value={formData.image} onChange={(event) => handleChange("image", event.target.value)} /><br />
        <TextField sx={{ margin: 1 }} label="Sound" variant="standard" value={formData.sound} onChange={(event) => handleChange("sound", event.target.value)} /><br />
        <button>Add</button> <button onClick={(e) => {
          e.preventDefault()
          setShowForm(false)
        }}>Cancel</button>
      </form>
    </>
  )
}

function ShowError(error : string) {
  if(error !== ""){
    return Alert("warning", error);
  }
  return
}