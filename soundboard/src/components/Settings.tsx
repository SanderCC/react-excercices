import Checkbox from '@mui/material/Checkbox';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';

export default function Settings() {
  return(<div className="form">
    <form>
      <h3>Settings</h3>
      <FormGroup>
        <FormControlLabel control={<Checkbox value={getNameSetting()} onChange={(event) => window.localStorage.setItem("displayNames", event.target.value)} />} label="Use names" />
        <FormControlLabel control={<Checkbox defaultChecked value={getQuoteSetting()} onChange={(event) => window.localStorage.setItem("displayQuotes", event.target.value)} />} label="Use quotes" />
      </FormGroup>
    </form>
  </div>)
}

export function getNameSetting() {
  return window.localStorage.getItem("displayNames")
}

export function getQuoteSetting() {
  return window.localStorage.getItem("displayQuotes")
}