import './../css/Soundboard.css'
import { useState } from "react"
import { useGetCollection } from './../hooks/useGetCollection'
import { ItemForm } from './ItemForm'
import { Item } from './ISoundboard'
import SoundboardItem from './SoundboardItem'
import {
  useParams
} from "react-router-dom";
import Alert from "./Alert"

const defaultQuote = "Click on an item to listen"
type RouterParams = { boardId: string };

export function Soundboard() {
  const { boardId } = useParams<RouterParams>();
  const [quote, setQuote] = useState<string | null>(defaultQuote)
  const [showForm, setShowForm] = useState<boolean>(false)
  const { loading, data: items, refetch } = useGetCollection("/soundboards/"+boardId+"/items")
  
  if(loading) return Alert("info", "Loading...")
  
  if(showForm){
    return (
      <div className="form">
        <ItemForm key={boardId} soundboard={boardId} refetch={refetch} setShowForm={setShowForm} items={items} />
      </div>
    );
  }

  return (
    <>
      <div className="soundboard">
        {quote && <div className="quote">{quote}</div>}
        <div className="items">
          {items.map((item: Item) => (
          <SoundboardItem
            item={item}
            key={item.id}
            onSoundStart={(quote) => setQuote(quote)}
            onSoundStop={() => setQuote(defaultQuote)}
            />
          ))}
          <div 
              className="item" 
              onClick={() => setShowForm(true)}
            >
              <img alt="new item" src="../img/plus.png" />
            </div>
        </div>
      </div>
    </>
  );
}

export default Soundboard