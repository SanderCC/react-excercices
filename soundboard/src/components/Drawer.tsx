import * as React from 'react';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import Button from '@mui/material/Button';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import { useHistory } from "react-router-dom"

type Anchor = 'top' | 'left' | 'bottom' | 'right';

export default function TemporaryDrawer() {
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });
  const history = useHistory();

  const toggleDrawer =
    (anchor: Anchor, open: boolean) =>
    (event: React.KeyboardEvent | React.MouseEvent) => {
      if (
        event.type === 'keydown' &&
        ((event as React.KeyboardEvent).key === 'Tab' ||
          (event as React.KeyboardEvent).key === 'Shift')
      ) {
        return;
      }

      setState({ ...state, [anchor]: open });
    };

  const list = (anchor: Anchor) => (
    <Box
      sx={{ width: anchor === 'top' || anchor === 'bottom' ? 'auto' : 250 }}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
        {['Boards', 'Settings', 'About'].map((text, index) => (
          <ListItem button key={text}>
            <ListItemText primary={text} onClick={() => {
              history.push('/'+text);
              document.title = text;
            }} />
          </ListItem>
        ))}
      </List>
    </Box>
  );

  const align = "left"
  return (
    <>
      <React.Fragment key={align}>
        <Drawer
          anchor={align}
          open={state[align]}
          onClose={toggleDrawer(align, false)}
        >
          {list(align)}
        </Drawer>
      </React.Fragment>
      <Button sx={{m: 1, color: "white"}} onClick={toggleDrawer(align, true)}>Menu</Button>
    </>
  );
}