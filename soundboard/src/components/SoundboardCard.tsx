import * as React from 'react';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { useHistory } from "react-router-dom"

interface SoundboardCardProps {
  id: number,
  name: string,
  image: string
}

export function SoundboardCard({id, name, image} : SoundboardCardProps) {
  const history = useHistory();
  return (
    <Card onClick={ () => history.push("/board/"+id) }>
      <CardMedia
        component="img"
        height="194"
        image={image}
        alt={name}
      />
      <CardContent>
        <Typography align="center" variant="h5" component="div">
        {name}
        </Typography>
      </CardContent>
    </Card>
  );
}

export default SoundboardCard;