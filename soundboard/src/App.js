import Soundboard from "./components/Soundboard.tsx";
import Soundboards from "./components/Soundboards.tsx";
import Drawer from "./components/Drawer.tsx";
import Settings from "./components/Settings";
import About from "./components/About";
import { useState } from "react";

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

function App() {
  return (
    <Router>
      <div className="App">
        <Drawer />
        <h1>Soundboard</h1>
        <Switch>
          <Route path="/board/:boardId">
            <Soundboard />
          </Route>
          <Route path="/settings">
            <Settings />
          </Route>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/">
            <Soundboards />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App
